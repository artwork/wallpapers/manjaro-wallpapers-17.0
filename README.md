# official wallpapers for the Manjarolinux 17.0 release

A big thank you to the awesome Manjaro Community for their contributions,
namely **optics**, **Reznorix**, **edskeye** and **coco** for the wallpapers in this repo.

There are of course many more fantastic backgrounds to be found on the manjaro forums.
These here are from [**this**](https://forum.manjaro.org/t/manjaro-17-wallpapers/17276) dedicated thread.
